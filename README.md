ChevronMenu.cs

What is it?
-----------

A winforms user control that pains a Chevron menu, somewhat like this:

 __________   ___________  __________
 > Option 1 > > Option 2 > > Option 3 >
 ----------   ----------- -----------

I'm not a big fan of it, to be honest, but it was a request, and now you all can have it.

Customization
-------------
All customization is done through the user control's properties preference pane.

Installation
------------
Add the project to your solution, after a rebuild, it will automatically show up in your toolbox.

Licensing
----------
Please see the file called LICENSE.

Contact
-------
twitter --> @ald
G+ --> http://afraelortiz.com/+
github --> http://github.com/afrael

PS: I'll write a nicer README with a picture of the menu a bit later.
