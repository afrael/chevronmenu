﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PresentationControls
{
    public partial class ChevronMenu : UserControl
    {
        // Private inner classes
        private class ChevronGraphicInfo
        {
            public ChevronGraphicInfo() { }

            private bool _shadowApplied = false;

            // Properties
            public ChevronItem Chevron { get; set; }
            public Point[] ChevronPolygonShape { get; set; }
            public Rectangle ChevronTextRect { get; set; }
            public Graphics CurrentGraphics { get; set; }
            public float ShadowOffset { get; set; }
            public bool ApplyShadow { get; set; }
            public bool Selected { get; set; }
            public bool Enabled { get; set; }

            public bool IsLocationInChevronTextRect(Point cursorLocation)
            {
                return ChevronTextRect.Contains(cursorLocation);
            }

            public bool IsLocationInChevronPolygon(Point cursorLocation)
            {
                return ChevronPolygonShape.Contains(cursorLocation);
            }

            public void PaintChevron(Color chevronForeColor, Color chevronBackColor, Color chevronSelectedBackColor, Color chevronSelectedForeColor, Color chevronDisabledBackColor, Color chevronDisabledForeColor, Font chevronFont, Color chevronBorderColor, int chevronBorderWidth)
            {
                if (ApplyShadow && !_shadowApplied) PaintChevronShadow(CurrentGraphics, ChevronPolygonShape);

                var brushColor = chevronDisabledBackColor;
                var brushStringColor = new SolidBrush(chevronDisabledForeColor);

                if (Chevron.Enabled)
                {
                    brushColor = chevronBackColor;
                    brushStringColor = new SolidBrush(chevronForeColor);
                }

                if (Chevron.Selected)
                {
                    brushColor = chevronSelectedBackColor;
                    brushStringColor = new SolidBrush(chevronSelectedForeColor);
                }

                if (CurrentGraphics != null) CurrentGraphics.SmoothingMode = SmoothingMode.AntiAlias;

                var brush = new SolidBrush(brushColor);

                CurrentGraphics.FillPolygon(brush, ChevronPolygonShape);
                var pen = new Pen(chevronBorderColor, chevronBorderWidth);
                CurrentGraphics.DrawPolygon(pen, ChevronPolygonShape);
                var stringFormat = new StringFormat
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };
                CurrentGraphics.DrawString(Chevron.Value, chevronFont, brushStringColor, ChevronTextRect, stringFormat);
            }

            private void PaintChevronShadow(Graphics g, Point[] polygonPoints)
            {
                var path = new GraphicsPath();
                path.AddPolygon(polygonPoints);
                var matrix = new Matrix();
                matrix.Translate(ShadowOffset, ShadowOffset + 2);
                path.Transform(matrix);
                using (var pathBrush = new PathGradientBrush(path))
                {
                    pathBrush.WrapMode = WrapMode.Clamp;
                    var colorBlend = new ColorBlend(3)
                    {
                        Colors =
                            new[]
                                {
                                    Color.Transparent, Color.FromArgb(180, Color.DimGray),
                                    Color.FromArgb(180, Color.DimGray)
                                },
                        Positions = new[] { 0f, .1f, 1f }
                    };
                    pathBrush.InterpolationColors = colorBlend;
                    g.FillPath(pathBrush, path);
                }
                _shadowApplied = true;
            }

        }
        private class ChevronGraphicInfoCollection : CollectionBase
        {
            public ChevronGraphicInfo this[int Index]
            {
                get
                {
                    if (Index == -1) return null;
                    return (ChevronGraphicInfo)List[Index];
                }
            }

            public bool Contains(ChevronGraphicInfo itemType)
            {
                return List.Contains(itemType);
            }

            public bool Contains(ChevronItem chevronItem)
            {
                return List.Cast<ChevronGraphicInfo>().Any(item => item.Chevron == chevronItem);
            }

            public int Add(ChevronGraphicInfo itemType)
            {
                return List.Add(itemType);
            }

            public void Remove(ChevronGraphicInfo itemType)
            {
                List.Remove(itemType);
            }

            public void Insert(int index, ChevronGraphicInfo itemType)
            {
                List.Insert(index, itemType);
            }

            public int IndexOf(ChevronGraphicInfo itemType)
            {
                return List.IndexOf(itemType);
            }

            public int IndexOf(ChevronItem chevronItem)
            {
                foreach (ChevronGraphicInfo chevronGraphicInfo in List)
                {
                    if (chevronGraphicInfo.Chevron == chevronItem)
                        return IndexOf(chevronGraphicInfo);
                }
                return -1;
            }
        }

        // Public Event and Custom Event Argument
        public class ChevronItemEventArgs : EventArgs
        {
            public ChevronItem SelectedChevron { get; set; }
        }

        [Category("Action"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public event EventHandler<ChevronItemEventArgs> ChevronClick;

        private ChevronItemCollection _itemCollection = new ChevronItemCollection();
        private ChevronGraphicInfoCollection _chevronGraphicInfos = new ChevronGraphicInfoCollection();
        private ChevronItem _activeChevron = null;
        private ChevronItem _hoveredChevron = null;

        private int _selectedIndex;
        private string _selectedValue;
        private ChevronItem _selectedItem;
        private ChevronItem _selectingItem;

        private Color _chevronBorderColor = Color.SlateGray;
        private int _chevronBorderWidth = 0;

        private Color _chevronBackColor = Color.SlateGray;
        private Color _chevronSelectedBackColor = Color.DarkSlateGray;
        private Color _chevronDisabledBackColor = Color.Gainsboro;

        private Color _chevronForeColor = Color.White;
        private Color _chevronSelectedForeColor = Color.White;
        private Color _chevronDisabledForeColor = Color.White;

        private Font _chevronFont = new Font("Microsoft Sans Serif", 8.25f);
        private Font _chevronSelectedFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
        private Font _chevronDisabledFont = new Font("Microsoft Sans Serif", 8.25f);

        // Properties
        [Category("Behavior"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ChevronItemCollection Items
        {
            get { return _itemCollection; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Color ChevronBackColor
        {
            get { return _chevronBackColor; }
            set { _chevronBackColor = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Color ChevronSelectedBackColor
        {
            get { return _chevronSelectedBackColor; }
            set { _chevronSelectedBackColor = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Color ChevronDisabledBackColor
        {
            get { return _chevronDisabledBackColor; }
            set { _chevronDisabledBackColor = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Color ChevronForeColor
        {
            get { return _chevronForeColor; }
            set { _chevronForeColor = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Color ChevronSelectedForeColor
        {
            get { return _chevronSelectedForeColor; }
            set { _chevronSelectedForeColor = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Color ChevronDisabledForeColor
        {
            get { return _chevronDisabledForeColor; }
            set { _chevronDisabledForeColor = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Font ChevronFont
        {
            get { return _chevronFont; }
            set { _chevronFont = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Font ChevronSelectedFont
        {
            get { return _chevronSelectedFont; }
            set { _chevronSelectedFont = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Font ChevronDisabledFont
        {
            get { return _chevronDisabledFont; }
            set { _chevronDisabledFont = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Color ChevronBorderColor
        {
            get { return _chevronBorderColor; }
            set { _chevronBorderColor = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public int ChevronBorderWidth
        {
            get { return _chevronBorderWidth; }
            set { _chevronBorderWidth = value; }
        }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(true)]
        public bool ApplyShadow { get; set; }

        [Category("Appearance"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), DefaultValue(5f)]
        public float ShadowOffset { get; set; }

        [Category("Behavior"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                if (_itemCollection.IndexOf(_itemCollection[value]) != -1)
                {
                    _selectedIndex = value;
                    var chevronItem = _itemCollection[value];
                    chevronItem.Selected = true;
                    foreach (ChevronGraphicInfo cgi in _chevronGraphicInfos)
                    {
                        cgi.Chevron.Selected = cgi.Chevron == _itemCollection[value];
                    }
                    RepaintChevrons();
                }
            }
        }

        public string SelectedValue
        {
            get
            {
                _selectedValue = _itemCollection[_selectedIndex].Value;
                return _selectedValue;
            }
        }

        public ChevronItem SelectedItem
        {
            get
            {
                _selectedItem = _itemCollection[_selectedIndex];
                return _selectedItem;
            }
        }

        public ChevronMenu()
        {
            InitializeComponent();
            ApplyShadow = true;
        }

        // Events

        private void OnPaint(object sender, PaintEventArgs e)
        {
            if (_itemCollection.Count == 0) return;
            if (e.Graphics == null) return;
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            PaintChevrons(e.Graphics);
        }

        private void OnResize(object sender, EventArgs e)
        {
            if (_itemCollection.Count == 0) return;
            var g = CreateGraphics();
            g.SmoothingMode = SmoothingMode.AntiAlias;
            PaintChevrons(g);
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            _selectingItem = GetChevronInLocation(e.Location);
        }

        private void OnMouseEnter(object sender, EventArgs e)
        {
        }

        private void OnMouseHover(object sender, EventArgs e)
        {
        }

        private void OnMouseLeave(object sender, EventArgs e)
        {
            RepaintChevrons();
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (IsMouseOverChevron(e.Location))
            {
                Cursor.Current = Cursors.Hand;
            }
            else
            {
                Cursor.Current = Cursors.Hand;
                RepaintChevrons();
            }
        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            var mousedUpChevron = GetChevronInLocation(e.Location);
            if (mousedUpChevron == _selectingItem)
            {
                foreach (ChevronGraphicInfo cgi in _chevronGraphicInfos)
                {
                    cgi.Chevron.Selected = cgi.Chevron == mousedUpChevron;
                }
                var index = _itemCollection.IndexOf(mousedUpChevron);
                SelectedIndex = index;
            }
            RepaintChevrons();
            if (ChevronClick != null) ChevronClick(sender, new ChevronItemEventArgs { SelectedChevron = mousedUpChevron });
        }

        // Private methods

        private void PaintChevrons(Graphics graphics)
        {
            var chevronIndex = 0;
            var chevronHeight = Height - 4;
            var chevronWidth = (Width / _itemCollection.Count);
            var originCoordinates = new Point { X = 0, Y = 0 };
            foreach (var chevron in Items)
            {
                var polygonPoints = new[] 
                    { 
                        new Point {X = originCoordinates.X + (chevronIndex * chevronWidth + 4), Y = originCoordinates.Y },
                        new Point {X = originCoordinates.X + ((chevronIndex + 1) * chevronWidth + 4) - 30, Y = originCoordinates.Y },
                        new Point {X = originCoordinates.X + ((chevronIndex + 1) * chevronWidth + 4), Y = chevronHeight/2 },
                        new Point {X = originCoordinates.X + ((chevronIndex + 1) * chevronWidth + 4) - 30, Y = chevronHeight },
                        new Point {X = originCoordinates.X + (chevronIndex * chevronWidth + 4), Y = chevronHeight },
                        new Point {X = originCoordinates.X + (chevronIndex * chevronWidth + 4) + 30, Y = chevronHeight/2 }
                    };

                var rectanglePoints = new Rectangle
                    (
                        originCoordinates.X + (chevronIndex * chevronWidth + 4),
                        originCoordinates.Y,
                        chevronWidth,
                        chevronHeight
                    );

                PaintIndividualChevron(graphics, polygonPoints, rectanglePoints, (ChevronItem)chevron);
                chevronIndex++;
                originCoordinates = new Point { X = 0 - (chevronIndex * 20), Y = 0 };
            }
        }

        private void PaintIndividualChevron(Graphics g, Point[] polygonPoints, Rectangle textRectagle, ChevronItem item)
        {
            if (!_chevronGraphicInfos.Contains(item))
            {
                var chevronGInfo = new ChevronGraphicInfo
                {
                    Chevron = item,
                    ChevronPolygonShape = polygonPoints,
                    ChevronTextRect = textRectagle,
                    CurrentGraphics = g,
                    ShadowOffset = ShadowOffset,
                    ApplyShadow = ApplyShadow,
                    Selected = item.Selected,
                    Enabled = item.Enabled
                };
                _chevronGraphicInfos.Add(chevronGInfo);
            }
            var chevronGraphicInfo = _chevronGraphicInfos[_chevronGraphicInfos.IndexOf(item)];
            chevronGraphicInfo.PaintChevron(ChevronForeColor, ChevronBackColor, ChevronSelectedBackColor, ChevronSelectedForeColor, ChevronDisabledBackColor, ChevronDisabledForeColor, ChevronFont, ChevronBorderColor, ChevronBorderWidth);
        }

        private void RepaintChevrons()
        {
            foreach (ChevronGraphicInfo cgi in _chevronGraphicInfos)
            {
                cgi.PaintChevron(ChevronForeColor, ChevronBackColor, ChevronSelectedBackColor, ChevronSelectedForeColor, ChevronDisabledBackColor, ChevronDisabledForeColor, ChevronFont, ChevronBorderColor, ChevronBorderWidth);
            }
        }

        private void RepaintPreviousChevron(ChevronItem chevronItem)
        {
            var chevronGraphicInfo = _chevronGraphicInfos[_chevronGraphicInfos.IndexOf(chevronItem)];
            chevronGraphicInfo.PaintChevron(ChevronForeColor, ChevronBackColor, ChevronSelectedBackColor, ChevronSelectedForeColor, ChevronDisabledBackColor, ChevronDisabledForeColor, ChevronFont, ChevronBorderColor, ChevronBorderWidth);
        }

        private void RepaintChevronWithSelectedFont(ChevronGraphicInfo cgi)
        {
            var chevronGraphicInfo = _chevronGraphicInfos[_chevronGraphicInfos.IndexOf(cgi.Chevron)];
            chevronGraphicInfo.PaintChevron(ChevronForeColor, ChevronBackColor, ChevronSelectedBackColor, ChevronSelectedForeColor, ChevronDisabledBackColor, ChevronDisabledForeColor, ChevronSelectedFont, ChevronBorderColor, ChevronBorderWidth);
        }

        private bool IsMouseOverChevron(Point cursorLocation)
        {
            foreach (ChevronGraphicInfo cgi in _chevronGraphicInfos)
            {
                var isHovering = cgi.IsLocationInChevronTextRect(cursorLocation);
                if (isHovering)
                {
                    _hoveredChevron = cgi.Chevron;
                    if (_hoveredChevron != _activeChevron)
                    {
                        if (_activeChevron == null) _activeChevron = cgi.Chevron;
                        RepaintPreviousChevron(_activeChevron);
                    }
                    RepaintChevronWithSelectedFont(cgi);
                    _activeChevron = _hoveredChevron;
                    return true;
                }
            }
            return false;
        }

        private ChevronItem GetChevronInLocation(Point cursorLocation)
        {
            foreach (ChevronGraphicInfo cgi in _chevronGraphicInfos)
            {
                var isHovering = cgi.IsLocationInChevronTextRect(cursorLocation);
                if (isHovering)
                {
                    return cgi.Chevron;
                }
            }
            return null;
        }

    }

    // Public Internal Classes

    public class ChevronItem
    {
        public ChevronItem()
        {
            Enabled = true;
        }

        [Category("Behavior")]
        public int Key { get; set; }
        [Category("Behavior")]
        public string Value { get; set; }
        [Category("Behavior"), DefaultValue(true)]
        public bool Enabled { get; set; }
        [Category("Behavior"), DefaultValue(false)]
        public bool Selected { get; set; }
    }

    public class ChevronItemCollection : CollectionBase
    {
        public ChevronItem this[int Index]
        {
            get
            {
                if (Index == -1) return null;
                return (ChevronItem)List[Index];
            }
        }

        public bool Contains(ChevronItem itemType)
        {
            return List.Contains(itemType);
        }

        public int Add(ChevronItem itemType)
        {
            return List.Add(itemType);
        }

        public void Remove(ChevronItem itemType)
        {
            List.Remove(itemType);
        }

        public void Insert(int index, ChevronItem itemType)
        {
            List.Insert(index, itemType);
        }

        public int IndexOf(ChevronItem itemType)
        {
            return List.IndexOf(itemType);
        }
    }
}
